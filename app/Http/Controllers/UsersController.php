<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Department;
use App\Candidate;
use App\Role;
use App\Userrole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;
 

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $departments = Department::all();
        $roles = Role::all();
        return view('users.index', compact('users','departments','roles'));
    }

    
    public function changeDepartment($uid,$did){
        $user = User::findOrFail($uid);
        if($user->candidates->count() == 0) {
            $user->department_id = $did;
            $user->save();
        }
        else {
            Session::flash('notallowed', 'You cannot change department! the user is owner of candidate');
        }
        return back();
    }

    public function makeManager($uid){
        if(Auth::user()->isAdmin()){
            $userrole = new Userrole();
            $userrole->user_id = $uid;
            $userrole->role_id = 2;
            $userrole->save();
            Session::flash('message','the user change to manager ');
        }
        return redirect('users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('users.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->department_id = $request->department_id;
        $user->save();
        return redirect('candidates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();
        return view('users.edit', compact('user','departments'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if(!isset($request->password)){
            $request['password'] = $user->password;   
        }else{
            $request['password'] = Hash::make($request['password']);
        } 
        $user->update($request->all());
        return redirect('users');  

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('is-admin'); 
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users'); 

    }
}
