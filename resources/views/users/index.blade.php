@extends('layouts.app')
@section('content')
@section('title', 'Users')
        <h1>List of Users</h1>
        <table class = "table table-dark">
            <tr> 
                <th>Id</th><th>Name</th><th>Email</th><th>Department</th><th>Roles</th><th>Change</th><th>Created</th><th>Updated</th>
            </tr>
        @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
            <td>
                    @foreach ($user->roles as $role)
                        {{ $role->name }}
                    @endforeach
                </td>
                <td>
                @if(!$user->isManager() )
                <a href = "{{route('user.makemanager',$user->id)}}" class="btn btn-primary">Make Manager</a>
                @endif
                @if($user->isManager() )
                <a href = "{{route('userroles.delete',$user->id)}} " class="btn btn-primary">Cancel Manager</a>
                @endif
                </td>
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td>
                <a href = "{{route('users.edit',$user->id)}}">Edit</a>
            </td>
            <td>
                    <a href = "{{route('user.show',$user->id)}}">Details</a>
            </td>  

            <td>
                <a href = "{{route('users.delete',$user->id)}}">Delete</a> 
            </td> 

        </tr>
        @endforeach
        </table>
@endsection
