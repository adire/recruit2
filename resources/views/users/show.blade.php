@extends('layouts.app')

@section('title', 'users')

@section('content')

    <h1>User Details</h1>
    <table class = "table table-dark">
        <tr>
            <td>Name</td>
            <td>{{$user->name}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$user->email}}</td>
        </tr> 
        <tr>   
            <td>Department</td>
            <td>{{$user->department->name}}</td>       
        </tr>
 
        <tr>
            <td>Created</td>
            <td>{{$user->created_at}}</td>
        </tr>
        <tr>
            <td>Updated</td>
            <td>{{$user->updated_at}}</td>  
        </tr>    
    </table>
@endsection
